// SERVIDOR WEB
var express = require('express')
	//serveStatic = require('serve-static'),
var s = require('./class/server')
var app = express()
var sb = require('spellbook')

app.use("/", express.static('web'));
//app.use(serveStatic('./web', {'index': ['index.html', 'index.htm']}))
app.listen(8080, function () {
	console.log("Rabbit server running in: 8080")
})

var io = require('socket.io')({
	transports: ['websocket'],
})

io.attach(4567)

io.on('connection', function(socket){
	s.newStats(socket.id + " -> CONNECTION")

	// WEB
	socket.on ('stats', function () {
		s.mkWebStats()
		socket.emit('stats', { msg : s.misweb, last : s.laststats })
	})

	socket.on ('reset', function () {
		s.clients.clear()
		s.rooms.clear()
		s.newStats(" !!!! RESET !!!!")
	})

	socket.on ('clear', function () {
		s.laststats.clear()
	})

	// PARTIDA NOVA
	socket.on('new', function (data) {
		var userpos = sb.random(s.initpos.min, s.initpos.max)
		if (s.rooms.size == 0) {
			s.rooms.set(1, [{ id : socket.id, nick : data.nick, position : [userpos, 6.2] }])
			s.clients.set(socket.id, { socket : socket, room : 1 })
			s.clients.get(socket.id).socket.emit('newResp', { id : socket.id, sala : 1, users : [], initpos : userpos})
		} else {
			var nosala = true
			for (var i = 1; i <= s.rooms.size; i++) {
				if (s.rooms.has(i)) {
					if (s.rooms.get(i).length < 4) {
						s.rooms.get(i).push({ id : socket.id, nick : data.nick, position : [userpos, 6.2] })
						s.clients.set(socket.id, { socket : socket, room : i})
						s.clients.get(socket.id).socket.emit('newResp', { id : socket.id, sala : i, users : s.rooms.get(i), initpos : userpos  })
                    for (var i = 0; i < s.rooms.get(s.clients.get(socket.id).room).length; i++) {
                    	if (s.rooms.get(s.clients.get(socket.id).room)[i].id !== socket.id) s.clients.get(s.rooms.get(s.clients.get(socket.id).room)[i].id).socket.emit('newPlayer', { id : socket.id, nick : data.nick, position : [userpos, 6.2] })
		                }
						nosala = false
						i = s.rooms.size
					}
				}
			}
			if (nosala) {
				var roomdef = s.rooms.size + 1
				s.rooms.set(roomdef, [{ id : socket.id, nick : data.nick, position : [userpos, 6.2] }])
				s.clients.set(socket.id, { socket : socket, room : roomdef})
				s.clients.get(socket.id).socket.emit('newResp', { id : socket.id, sala : roomdef, users : [], initpos : userpos })
			}
		}
		s.newStats("NEW: " + socket.id + " -> " + data.nick)
	})

	// UPDATE COORDENADES
	socket.on('updatePos', function (data) {
		var sala = []
		s.rooms.get(s.clients.get(socket.id).room).map(function (obj) {
			if (obj.id !== socket.id) sala.push(obj)
			else {
				obj.position = [data.x, data.y]
				sala.push(obj)
			}
		})
		s.rooms.set(s.clients.get(socket.id).room, sala)
		for (var i = 0; i < s.rooms.get(s.clients.get(socket.id).room).length; i++) {
			s.clients.get(s.rooms.get(s.clients.get(socket.id).room)[i].id).socket.emit('allPos', { room : s.rooms.get(s.clients.get(socket.id).room) })
		}
		s.newStats(socket.id + " -> updatePos")
	})

	// DESCONEXIONS
	socket.on('disconnect', function(){
		s.newStats(socket.id + " -> DISCONNECT")
		if ( !sb.empty(s.rooms) || !sb.empty(s.clients)) {
			s.clients.forEach(function (value, key, map) {
				if (key === socket.id) {
					var sala = []
					s.rooms.get(s.clients.get(socket.id).room).map(function (obj) {
						if ( obj.id !== socket.id ) sala.push(obj)
					})
					if (sala.length > 0) s.rooms.set(s.clients.get(socket.id).room, sala)
					else s.rooms.delete(s.clients.get(socket.id).room)
					s.clients.delete(socket.id)
				}
			})
		}
	})
})
