module.exports = {
	clients : new Map(),
	rooms : new Map(),
	laststats : [],
	initpos : { max : 8.5, min : -8.5 },
	misweb : "",
	checkRooms : function () {
		if (this.rooms.size > 0) {
			console.log("")
			console.log("------ STATS -----")
			console.log("Cantitat de rooms : " + this.rooms.size)
			for (var i = 1; i <= this.rooms.size;  i++) {
				if (this.rooms.has(i)) console.log("SALA: " + i + " -> USUARIS: " + this.rooms.get(i).length + " -- " + JSON.stringify(this.rooms.get(i)))
			}
			console.log("---- STATS END ---")
			console.log("")
		}
	},
	newStats : function (message) {
		if (this.laststats.length > 10) this.laststats.shift()
		this.laststats.push( new Date() + " -- " + message)
		console.log(message)
	},
	mkWebStats : function () {
		this.misweb = "<b>SERVIDOR:</b> Sense usuaris!"
	    if (this.rooms.size > 0) {
            this.misweb = "<b>Cantitat de rooms:</b> " + this.rooms.size + "<br>"
            for (var i = 1; i <= this.rooms.size;  i++) {
                if (this.rooms.has(i)) this.misweb = this.misweb + "<b>SALA:</b> " + i + " -> <b>USUARIS:</b> " + this.rooms.get(i).length + " -- " + JSON.stringify(this.rooms.get(i)) + "<br>"
            }
        }
    }
}
