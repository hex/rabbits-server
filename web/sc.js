var socket = io.connect('http://' + window.location.hostname + ':4567')

function refresh(){
        socket.emit('stats');
        setTimeout(refresh, 1000);
}

function reset() {
	socket.emit('reset');
}

function newuser(min, max) {
	socket.emit('new', { nick : Math.floor(Math.random() * (max - min + 1)) + min });
}

socket.on('stats', function(data){
	$('#stats').html(data.msg);
	$('#laststats').html(data.last.join('<br>'));
});

$(document).ready(function() {
	refresh();
	$('#reset').click(function () {
		reset();
	});

	$('#new').click(function () {
		newuser(5555, 9999);
	});

	$('#connect').click(function () {
		socket.connect();
	});

	$('#disconnect').click(function () {
		socket.disconnect();
	});

	$('#clear').click(function () {
		socket.emit('clear');
	});
});
